#![no_std]
#![no_main]

extern crate bootloader;

use core::panic::PanicInfo;
use vga::{println};
use interrupts;

use platinum_os::hlt_loop;

#[panic_handler]
fn panic(info: &PanicInfo) -> ! {
	println!("{}", info);

	hlt_loop()
}

#[no_mangle]
pub fn _start() -> ! {
	println!("Welcome to {}", "Platinum OS");

	interrupts::init_idt();

	println!("It did not crash");

	hlt_loop()
}